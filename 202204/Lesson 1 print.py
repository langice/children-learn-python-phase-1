#!/usr/bin/python
# coding=utf-8
import sys

# 输出一串字符
print("你好，世界")
print('abcdefg')

# 输出一个数字
print(5)

# 输出一个表达式
print(3 * 5)

# 输出一个变量
a = 1
print(a)

# 一个输出多个结果
print("a", "b")

# 用符号替换空格
print("赵志轩", "李锦川", sep=",")

# 修改输出结尾
print("山西朔州", end="朔城区")

# print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)
